console.log('Hello, World')
// Details
const details = {
  fName: "Federico",
  lName: "Catabay",
  age: 19,
  hobbies: [
        "playing", "eating", "lifting weights"
    ],
  workAddress: {
    housenumber: "Lot 10, Block 5",
    street: "Balgan Ville",
    city: "Caloocan City",
    state: "Philippines",
  }
}
const work = Object.values(details.workAddress);
console.log("My First Name is " + details.fName)
console.log("My Last Name is " + details.lName)
console.log(`Yes, I am ${details.fName} ${details.lName}.`)
console.log("I am " + details.age + " years old.")
console.log(`My hobbies are ${details.hobbies.join(', ')}.`);
console.log("I work at " + work.join(", ") + ".");